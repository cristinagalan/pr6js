window.addEventListener("load", () => {
    /* **************************************************** FLORES ****************************************************  */
    var fotosFlores = new Array();
    var imagenesflores = new Array();
     var imagenesPeque= new Array();

    /* Lleno el array con las fotos */
    for (var i = 0; i < 5; i++) {
        fotosFlores[i] = (i + 1) + ".jpg";
    }


    /* ********** AÑADIR PÁGINAS **********   */

    var centro = document.querySelector(".celda2:nth-child(2)");
    var npaginas = Math.ceil(fotosFlores.length / 4);
    for (var i = 0; i < npaginas; i++) {
        centro.innerHTML += "<a href='#' data-indice=" + i + "'>" + (i + 1) + "</a>";
    }

    /* ********** EJECUTAR PÁGINAS **********   */
 
	var enlaces=document.querySelectorAll(".celda2 a");
    console.log(enlaces);

	enlaces.forEach((va)=>{
		va.addEventListener("click",(event)=>{
			var objeto=event.target;
			var indice=objeto.getAttribute("data-indice");

			var aux = 0;
            var longitud=fotosFlores.length-1;
			aux=parseInt(indice)*4;
	        for (var i = 0; i < 4; i++,aux++) {
	               // document.querySelectorAll(".cards")[i].src = "imgs/flowers/thumbs/" + fotosFlores[aux];
            
                   if(aux>longitud){
                    document.querySelectorAll(".cards")[i].src="";
                   }else{
                    document.querySelectorAll(".cards")[i].src =imagenesPeque[aux].src; 
                   }
                   
	        }
	        
	    });
	});


    /* ********** Precargo imágenes grandes **********  */

    for (var i = 0; i < fotosFlores.length; i++) {
        imagenesflores[i] = new Image();
        imagenesflores[i].src = "imgs/flowers/" + fotosFlores[i];
    }

    /* **********  Precargo imágenes pequeñas **********  */

        for(var i=0;i<fotosFlores.length;i++){
            imagenesPeque[i]=new Image();
            imagenesPeque[i].src="imgs/flowers/thumbs/" + fotosFlores[i];
        }
        for (var i = 0; i < 4; i++) {
            document.querySelectorAll(".cards")[i].src = imagenesPeque[i].src;
        }



    /* Ir a página anterior */

    /* Ir a página siguiente */


    /* ********** imagen por defecto ********** */

    document.querySelector(".mediana").src = imagenesflores[0].src;

    /* **********  EJECUTAR 1 ********** */

    for (var i = 0; i < 4; i++) {
        document.querySelectorAll(".cards")[i].onclick = ejecutar1;
    }

    function ejecutar1(event) {
        var indice = event.target.getAttribute("data-indice");
        var caja = document.querySelector(".foto");
        caja.style.backgroundImage = "url('" + imagenesflores[indice].src + "')";
        caja.style.backgroundSize = "cover";
        document.querySelector(".mediana").src = imagenesflores[indice].src;

    }


    /* ********** Poner imagen mediana **********   */
    document.querySelector(".foto").onclick = ejecutar2;
    document.querySelector(".ocultar").style.display = "none";
    document.querySelector(".ocultar").onclick = cerrar;

    function ejecutar2(event) {
        var indice = event.target.getAttribute("data-indice");
        var caja = document.querySelector(".ocultar");
        caja.style.backgroundImage = "url('" + document.querySelector(".mediana").src + "')";

        caja.style.display = "block";
    }
    /* **********  Cerrar **********  */
    function cerrar() {
        document.querySelector(".ocultar").style.display = "none";
        document.querySelector(".ocultar").style.backgroundImage = "none";
    }


});